<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $produtos->id !!}</p>
</div>

<!-- Nome Field -->
<div class="form-group">
    {!! Form::label('nome', 'Nome:') !!}
    <p>{!! $produtos->nome !!}</p>
</div>

<!-- Preco Field -->
<div class="form-group">
    {!! Form::label('preco', 'Preço:') !!}
    <p>{!! $produtos->preco !!}</p>
</div>

<!-- Descricao Field -->
<div class="form-group">
    {!! Form::label('descricao', 'Descrição:') !!}
    <p>{!! $produtos->descricao !!}</p>
</div>

<!-- Categoria Id Field -->
<div class="form-group">
    {!! Form::label('categoria_id', 'Categoria:') !!}
    <p>{!! $produtos->categoria ? $produtos->categoria->nome : 'Sem categoria' !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Criado em:') !!}
    <p>{!! date('d/m/Y H:i:s', strtotime($produtos->created_at)) !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Atualizado em:') !!}
    <p>{!! date('d/m/Y H:i:s', strtotime($produtos->updated_at)) !!}</p>
</div>

