<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Produtos
 * @package App\Models
 * @version July 13, 2019, 8:53 pm UTC
 *
 * @property \App\Models\Categorias categoria
 * @property string nome
 * @property float preco
 * @property string descricao
 * @property integer categoria_id
 */
class Produtos extends Model
{
    use SoftDeletes;

    public $table = 'produtos';
    

    protected $dates = ['deleted_at'];


    public $fillable = [
        'nome',
        'preco',
        'descricao',
        'categoria_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'nome' => 'string',
        'preco' => 'float',
        'descricao' => 'string',
        'categoria_id' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'nome' => 'required',
        'preco' => 'required',
        'descricao' => 'required'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function categoria()
    {
        return $this->belongsTo(\App\Models\Categorias::class, 'categoria_id', 'id');
    }
}
